require("dotenv").config();

// API instance
const api = require("express")();

const port = process.env.PORT ?? 443;

// Initialize AstridLogger
const { Logger: AstridLogger } = require("astridlogger");
global.logger = new AstridLogger(require("./logger.json"));
global.pkg = require("../package.json");

const debug = (msg) =>
  logger.debug({
    messages: [msg]
  });

// Allow JSON in bodies
api.use(require("express").json());
debug(`Starting up ${pkg.name} v${pkg.version}`);

// add powered by header
api.use(async (req, res, next) => {
  res.setHeader("X-Powered-By", pkg.name);
  next();
});

// Initialize router
// noinspection JSCheckFunctionSignatures
api.use("/v1", require("./Router"));

// anything after that is an invalid path

// Invalid path err handler
api.use((req, res) => {
  res.status(404).json({ error: "Path not found" });
});

// Misc error handling to avoid crashes
api.use((error, req, res) => {
  if (error.status) res.status(error.status);
  else res.status(500);
  res.json({
    message: error.message,
    stack: "🥞",
    status: "fail",
  });
});

debug(`Routes successfully created`);

// Start listening
api.listen(process.env.PORT ?? 443, () => {
  debug(`API for ${pkg.name} running on port ${port} `);
});

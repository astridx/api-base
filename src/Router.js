// Dependencies
const express = require("express");
const router = express.Router();
const cors = require("cors");

//Enable cors for this router
router.use(cors({ methods: ["GET", "POST", "DELETE"] }));

const main = require("./routes/main");

router.use("/", main);

module.exports = router;
